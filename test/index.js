'use strict'

var tape = require('tape')
var stream = require('stream')
var LowerCaseTransform = require('../')

tape('lower-case-it', function (t) {
  t.plan(1)
  var lowerTform = new LowerCaseTransform()
  var verifier = new stream.Writable
  var INPUT = 'aBc DeF -asd;198pryuasdjknfashf2-1asd/.fj G '
  verifier._write = function (data) {
    t.equals(data.toString(), INPUT.toLowerCase(), 'transformed ok')
    t.end()
  }
  lowerTform.pipe(verifier)
  lowerTform.write(INPUT)
  lowerTform.end()
})
