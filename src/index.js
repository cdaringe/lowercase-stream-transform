'use strict'

var Transform = require('stream').Transform
var util = require('util')

function LowerCaseTransform (opts) {
  if (!(this instanceof LowerCaseTransform)) return new LowerCaseTransform(opts)
  Transform.call(this, opts)
}
util.inherits(LowerCaseTransform, Transform)

LowerCaseTransform.prototype._transform = function (chunk, encoding, cb) {
  this.push(chunk.toString().toLowerCase())
  cb()
}

module.exports = LowerCaseTransform
