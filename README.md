# lowercase-stream-transform [![build status](https://gitlab.com/cdaringe/lowercase-stream-transform/badges/master/build.svg)](https://gitlab.com/cdaringe/lowercase-stream-transform/commits/master) ![](https://img.shields.io/badge/standardjs-%E2%9C%93-brightgreen.svg)

transforms a stream of string/buffers to lowercase text

## install

`npm install lowercase-stream-transform`

## usage

```js
var Lowercase = require('lowercase-stream-transform')
rs.pipe(new Lowercase()).pipe(ws)
rs.push('BANANAS')
// `ws` receives `bananas`
```

